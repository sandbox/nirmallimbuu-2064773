<?php

/**
 * Module settings page/form
 * 
 */
function override_templates_settings_form($form, $form_state) {
  global $theme_key;
  $weight = 0;
  $form = array();
  
  
  $save_themes = override_templates_read_themes_decode();
  $save_theme = variable_get('override_templates_theme_name', $theme_key);
  
  $form['theme_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Theme to use'),
      '#value' => $save_theme,
      '#weight' => $weight++,
  );
  
  $exploded_value = ($save_themes) ? explode(':', $save_themes) : false;
  $form['saved_values'] = array(
      '#type' => 'value',
      '#value' => $save_themes,
  );
  
  // 1. List of saved theme overrides
  //
  $html_list = t('List of theme overrides');
  if ($exploded_value) {
    $html_list .= '<ul>';
    $index = 0;
    foreach ($exploded_value as $anexplode) {
      $theme_detail = explode(',', $anexplode);
      $name = (count($theme_detail) > 1) ? $theme_detail[1] : $theme_detail[0];
      $html_list .= '<li id="theme-index-' . $index . '">' . '<label>' .  $theme_detail[0] . '</label>' . '<span>' . '<a id="theme-delete-' . $index . '" class="autod-delete" href="#" title="' . $name . '">' . t('(x)') . '</a>' . '</span></li>';
      $index++;
    }
    $html_list .= '</ul>';
  } else {
   $html_list .= '<div>';
   $html_list .= t('No theme override found');
   $html_list .= '</div>';
  }
  $form['theme_list'] = array(
      '#type' => 'markup',
      '#markup' => $html_list,
      '#weight' => $weight++,
  );
  
  // 2. enter new theme overrides
  //
  $form['themes'] = array(
      '#type' => 'textarea',
      '#value' => '',
      '#title' => 'enter the theme name to overwrite followed by its template name. For eg. <strong>themename,template-name</strong> . Provide different themes by separting with a : character. If you donot provide the template name, the template name of same theme name will be used. Do not use tpl.php in template name.',
      '#weight' => $weight++,
  );

  $form['save'] = array(
      '#type' => 'submit',
      '#value' => t('save'),
      '#weight' => $weight++,
  );

  return $form;
}

/**
 * Validate callback
 */
function override_templates_settings_form_validate($form, $form_state) {
  //
  $value = $form_state['values'];
  $input = $form_state['input'];

  if (empty($input['theme_name'])) {
    form_set_error('theme_name', t('You did not enter anything.'));
  }
  
  if (empty($input['themes'])) {
    form_set_error('themes', t('You did not enter anything.'));
  }
  
  $themes = $input['themes'];
  $input_themes = override_templates_read_themes_decode_array($themes);
  $keys = array_keys($input_themes);
  foreach ($keys as $akey) {
    if (theme_override_exists($akey)) {
      form_set_error('themes', t('You entere a theme which is already overridden. Theme: ') . $akey);
    }
  }
  
}

/**
 * Submit callback
 */
function override_templates_settings_form_submit($form, $form_state) {

  $value = $form_state['values'];
  $input = $form_state['input'];

  // get name of themes
  $themes = $input['themes'];
  // previously saved themes
  $old_themes = $form['saved_values']['#value'];
  
  
  // save theme name
  variable_set('override_templates_theme_name', $input['theme_name']);
  
  // read and save themes
  if (!empty($old_themes)) {
    variable_set('override_templates_read_themes_list', $old_themes . ':' . $themes);
  } else {
    variable_set('override_templates_read_themes_list', $themes);
  }
  
  // success message
  drupal_set_message('success');
}

/**
 * Read themes from form input
 * 
 * @param $data
 */
function override_templates_read_themes($themes) {
}


/**
 * Return themes name to its original format
 * 
 * @param $themes
 */
function override_templates_read_themes_decode() {
  $save_themes = variable_get('override_templates_read_themes_list', '');
  return $save_themes;
}

/**
 * Return themes name as associative array
 * 
 * @param $themes
 */
function override_templates_read_themes_decode_array($themes=NULL) {
  $save_themes = (!isset($themes)) ? variable_get('override_templates_read_themes_list', '') : $themes;
  $explode = explode(':', $save_themes);
  $return = array();
  foreach ($explode as $anexplode) {
    $theme_explode = explode(',', $anexplode);
    $theme_name = $theme_explode[0];
    $template_path = (count($theme_explode)>1) ? $theme_explode[1] : $theme_explode[0];
    $return[$theme_name] = array(
        'theme' => $theme_name,
        'template_path' => $template_path,
    );
  }
  return $return;
}



/**
 * Check whether the theme is already saved
 * 
 * @param $name
 *  The Name of the theme to check
 */
function theme_override_exists($name) {
  $all = override_templates_read_themes_decode_array();
  return (array_key_exists($name, $all)) ? true : false;
}


/**
 * Convert array of data into string
 * 
 * @param $themes
 */
function override_templates_read_themes_encode_array($themes) {
  //
  $list = '';
  $list_implode = array();
  foreach ($themes as $theme) {
    $list_implode[] = $theme['theme'] . ',' . $theme['template_path'];
  }
  $list = implode(':', $list_implode);
  return $list;
}



/**
 * Modal Form: confirmation action
 */
function override_templates_delete_confirm() {
  $index = $_GET['idx'];
  $name = $_GET['namex'];
  if ($index !== false) {
    // get currently saved
    $list = override_templates_read_themes_decode_array();
    $keys = array_keys($list);
    
    // check if theme name is correct
    if (in_array($name, $keys)) {
      
      // remove selected theme
      unset($list[$name]);
      
      // convert to string
      $list_str = override_templates_read_themes_encode_array($list);
    
      // save
      variable_set('override_templates_read_themes_list', $list_str);
    }
    
  }
}



