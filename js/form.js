/* 
 * 
 */


jQuery(document).ready(function($) {
  
  var links_delete = jQuery('.autod-delete');
  
  links_delete.click(function(e) {
    // get theme index
    var element_id = (jQuery(this).attr('id'));
    var explode = element_id.split('-');
    var id =  explode[2];
    // delete the override
    delete_theme_override(id);
  });
  
  
  /**
   * 
   * @param {int} index
   */
  function delete_theme_override(index) {
    var basePath = Drupal.settings.basePath;
    var name = jQuery('#theme-index-' + index + ' label').text();
    jQuery.ajax({
      type: 'GET',
      url: basePath + 'override_templates/delete/theme/' + index,
      data: {
        idx: index,
        namex: name,
      },
      dataType: 'html',
      success: function(data, status, request)
      {
        // remove link
        jQuery('#theme-index-' + index).remove();
      }
    });//end-ajax
  }
  
});


